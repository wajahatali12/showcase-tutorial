// ignore: file_names
// ignore_for_file: file_names

import 'package:flutter/material.dart';
import 'package:showcaseview/showcaseview.dart';

class Detail extends StatefulWidget {
  @override
  _DetailState createState() => _DetailState();
}

class _DetailState extends State<Detail> {
  final GlobalKey _typeHereKey = GlobalKey();
  final GlobalKey _sendKey = GlobalKey();
  final GlobalKey _appBarKey = GlobalKey();
  BuildContext? myContext;

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance!.addPostFrameCallback(
        (_) => Future.delayed(const Duration(milliseconds: 5), () {
              ShowCaseWidget.of(myContext!)!.startShowCase([_typeHereKey]);
            }));
  }

  @override
  Widget build(BuildContext context) {
    Future<void> _dialogBox() async {
      return showDialog(
        useSafeArea: false,
        context: context,
        barrierDismissible: true,
        barrierColor: Colors.black.withOpacity(0.6),
        builder: (BuildContext context) => Dialog(
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(20.0)),
          child: Container(
            height: 250.0,
            decoration: const BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.all(
                Radius.circular(20),
              ),
            ),
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 15),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  const SizedBox(height: 30),
                  SizedBox(
                    width: 100,
                    height: 100,
                    child: Image.asset(
                      'assets/1.png',
                      fit: BoxFit.contain,
                    ),
                  ),
                  const Text('Congratulations!'),
                  const SizedBox(height: 30),
                ],
              ),
            ),
          ),
        ),
      );
    }

    return ShowCaseWidget(
      builder: Builder(
        builder: (context) {
          myContext = context;
          return Scaffold(
            appBar: AppBar(
              backgroundColor: Colors.transparent,
              elevation: 0,
              leading: IconButton(
                icon: const Icon(
                  Icons.arrow_back,
                  color: Colors.black,
                ),
                onPressed: () {
                  Navigator.pop(context);
                },
              ),
              actions: [
                Showcase.withWidget(
                    disposeOnTap: true,
                    onTargetClick: () => _dialogBox(),
                    container: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        SizedBox(
                            width: 100,
                            height: 100,
                            child: Image.asset(
                              'assets/1.png',
                              fit: BoxFit.contain,
                            )),
                        // Icon(Icons.home_filled, color: Colors.white),
                        Container(
                          alignment: Alignment.center,
                          width: 100,
                          child: const Text(
                            'Tap Send',
                            textAlign: TextAlign.center,
                            style: TextStyle(color: Colors.white),
                          ),
                        )
                      ],
                    ),
                    height: 500,
                    width: 100,
                    key: _sendKey,
                    child: IconButton(
                      icon: const Icon(
                        Icons.send,
                        color: Colors.black,
                      ),
                      onPressed: () => _dialogBox(),
                    )),
              ],
            ),
            body: Padding(
              padding: const EdgeInsets.all(16.0),
              child: ListView(
                children: <Widget>[
                  Showcase.withWidget(
                    onTargetClick: () {},
                    disposeOnTap: true,
                    container: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        SizedBox(
                            width: 100,
                            height: 100,
                            child: Image.asset(
                              'assets/1.png',
                              fit: BoxFit.contain,
                            )),
                        // Icon(Icons.home_filled, color: Colors.white),
                        Container(
                          alignment: Alignment.center,
                          width: 100,
                          child: const Text(
                            'Type Here',
                            textAlign: TextAlign.center,
                            style: TextStyle(color: Colors.white),
                          ),
                        )
                      ],
                    ),
                    height: 500,
                    width: 100,
                    key: _typeHereKey,
                    child: TextField(
                      onSubmitted: (_) {
                        setState(() {
                          ShowCaseWidget.of(context)!.startShowCase([_sendKey]);
                        });
                      },
                      decoration: const InputDecoration(
                          hintText: 'Email Text',
                          labelStyle: TextStyle(
                            fontSize: 25,
                            fontWeight: FontWeight.w600,
                          )),
                    ),
                  ),
                  const SizedBox(
                    height: 16,
                  ),
                  // Text(
                  //   'Hi, you have new Notification from flutter group, open slack and check it out',
                  //   style: TextStyle(fontSize: 18, fontWeight: FontWeight.w500),
                  // ),
                  // SizedBox(
                  //   height: 16,
                  // ),
                  // RichText(
                  //   text: TextSpan(
                  //     style: TextStyle(
                  //         fontWeight: FontWeight.w400, color: Colors.black),
                  //     children: [
                  //       TextSpan(text: 'Hi team,\n\n'),
                  //       TextSpan(
                  //           text:
                  //               'As some of you know, we’re moving to Slack for our internal team communications. Slack is a messaging app where we can talk, share files, and work together. It also connects with tools we already use, like [add your examples here], plus 900+ other apps.\n\n'),
                  //       TextSpan(
                  //           text: 'Why are we moving to Slack?\n\n',
                  //           style: TextStyle(
                  //               fontWeight: FontWeight.w600,
                  //               color: Colors.black)),
                  //       TextSpan(
                  //           text:
                  //               'We want to use the best communication tools to make our lives easier and be more productive. Having everything in one place will help us work together better and faster, rather than jumping around between emails, IMs, texts and a bunch of other programs. Everything you share in Slack is automatically indexed and archived, creating a searchable archive of all our work.'),
                  //     ],
                  //   ),
                  // ),
                ],
              ),
            ),
          );
        },
      ),
    );
  }
}
